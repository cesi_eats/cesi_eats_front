import axios from "axios";
import {ArticlesGroupedByCategoryResponse, Restaurant, RestaurantResponse} from "@/services/types/DataTypes";



async function getAllRestaurants() : Promise<Restaurant[]> {
  const {data} = await axios.get<RestaurantResponse>("http://localhost:3000/api/restaurant/",
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return [];
}

async function getRestaurantById(restaurantId : string) : Promise<Restaurant[]> {
  const {data} = await axios.get<RestaurantResponse>("http://localhost:3000/api/restaurant/" + restaurantId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return [];
}

async function addRestaurant(formData : FormData) : Promise<boolean> {
  const {data} = await axios.post<RestaurantResponse>("http://localhost:3000/api/restaurant/owner/",
    formData,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!(data.success && data.data);
}

async function addNewCategory(restaurantId : string, categoryName : string) : Promise<boolean> {
  const {data} = await axios.post<ArticlesGroupedByCategoryResponse>("http://localhost:3000/api/category/owner/" + restaurantId,
    {
      name: categoryName
    },
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!(data.success && data.data);
}

async function removeCategory(categoryId : string) : Promise<boolean> {
const {data} = await axios.delete<ArticlesGroupedByCategoryResponse>("http://localhost:3000/api/category/owner/" + categoryId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!data.success;

}

export default {
  getAllRestaurants,
  getRestaurantById,
  addRestaurant,
  addNewCategory,
  removeCategory
} as const;
