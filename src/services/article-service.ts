import axios from "axios";
import {
  ArticlesCategoryId,
  ArticlesGroupedByCategory,
  ArticlesGroupedByCategoryResponse,
  Category
} from "@/services/types/DataTypes";


async function getArticlesByRestaurantId(restaurantId : string) : Promise<ArticlesGroupedByCategory> {
  const {data} = await axios.get<ArticlesGroupedByCategoryResponse>("http://localhost:3000/api/article/grouped/" + restaurantId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return {
    articlesByCategory: new Map<Category, ArticlesCategoryId>()
  };
}

async function addArticleToCategory(categoryId : string, formData : FormData) : Promise<boolean> {
  const {data} = await axios.post<ArticlesGroupedByCategoryResponse>("http://localhost:3000/api/article/owner/add/" + categoryId,
    formData,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!(data.success && data.data);

}

async function removeArticle(articleId : string){
const {data} = await axios.delete<ArticlesGroupedByCategoryResponse>("http://localhost:3000/api/article/owner/" + articleId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!(data.success && data.data);
}



export default {
  getArticlesByRestaurantId,
  addArticleToCategory,
  removeArticle
} as const;
