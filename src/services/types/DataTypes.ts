export type AuthResponse = {
  success: boolean;
  message: string;
  data?: {
    token: string;
  }
  status: number;
}

export type Article = {
  name: string;
  description: string;
  price: number;
  img?: string;
}

export type Category = {
  name: string;
  articles: Article[];
}

export type Card = {
  name: string;
  Categories: Category[];
}


export type Restaurant = {
  name: string;
  description: string;
  ownerId: number;
  category: string,
  img?: string;
  cards: Card[];
}

export type User = {
  firstName: string;
  name: string;
  email: string;
  roleId: string;
}

export type UserResponse = {
  status: number;
  success: string;
  message: string;
  data: User[];
}

export type Order = {
  userId: number,
  createdAt: Date,
  articles: Map<string,number>,
  delivererId: number,
  isDelivered: boolean,
}


export type OrderResponse = {
  success: boolean;
  data?: Order[];
}

export type DeliverOrderResponse = {
  success: boolean;
  data?: Order;
}


export type ArticleResponse = {
  success: boolean;
  data?: Article[];
}

export type ArticlesCategoryId = {
  categoryId: string;
  articles: Article[];
}

export type ArticlesGroupedByCategory = {
  articlesByCategory: Map<Category, ArticlesCategoryId>;
}

export type ArticlesGroupedByCategoryResponse = {
  success: boolean;
  data?: ArticlesGroupedByCategory;
}

export type CategoryResponse = {
  success: boolean;
  data?: Category[];
}

export type CardResponse = {
  success: boolean;
  data?: Card[];
}

export type RestaurantResponse = {
  success: boolean;
  data?: Restaurant[];
}

