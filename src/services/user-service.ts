import axios from "axios";
import {AuthResponse, User, UserResponse} from "@/services/types/DataTypes";
import {NavigationGuardNext, RouteLocationNormalized} from "vue-router";
import jwtDecode from 'jwt-decode';
import router from "@/router";


async function login(email: string, password: string) : Promise<boolean> {
  const {data} = await axios.post<AuthResponse>("http://localhost:3000/api/auth/login/", {
    email: email,
    password: password
  },{
    validateStatus: function (status) {
      return status >= 200 && status < 500
    }
  });
  if (data.success && data.data) {
    localStorage.setItem("token", data.data.token);
    return true;
  }
  return false;
}

function logout(){
  if (localStorage.getItem("token")) {
    localStorage.removeItem("token");
    router.push("/login");
    return true;
  }
  return false;
}

async function register(firstName: string, name : string, email: string, password: string) : Promise<boolean> {
  const {data} = await axios.post<AuthResponse>("http://localhost:3000/api/auth/register/", {
    firstName: firstName,
    name: name,
    email: email,
    password: password
  },{
    validateStatus: function (status) {
      return status >= 200 && status < 500
    }
  });
  return !!data.success;
}

function checkAuthenticationMiddleware(to : RouteLocationNormalized, from : RouteLocationNormalized, next: NavigationGuardNext) {
  if (!localStorage.getItem("token")) {
    router.push("/login");
    return;
  }
  // check for valid auth token
  checkAuthentication().then(authenticated => {
    if (!authenticated) {
      router.push("/login");
      return;
    }
    next();
  })
}

async function checkAuthentication() : Promise<boolean> {
  try {
    const response = await axios.post('http://localhost:3000/api/auth/isvalid', {
      token: localStorage.getItem("token")
    },{
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
    if (response.data.success) {
      return true;
    }
    localStorage.removeItem("token");
    return false;
  }
  catch (e) {
    return false;
  }
}

async function getAll() : Promise<User[]>{
  const {data} = await axios.get<UserResponse>("http://localhost:3000/api/auth/admin/all/", {
    validateStatus: function (status) {
      return status >= 200 && status < 500
    }
  });
  if (data.success && data.data) {
    return data.data;
  }
  return [];
}

async function deleteUser(userEmail : string){
  const {data} = await axios.post<AuthResponse>("http://localhost:3000/api/auth/admin/delete/", {
    email: userEmail
  },{
    validateStatus: function (status) {
      return status >= 200 && status < 500
    }
  });
  return !!data.success;
}

function getCurrentUserId() : number {
  if (localStorage.getItem("token")) {
    const token = localStorage.getItem("token");
    const decoded = jwtDecode(token);
    return decoded.id;
  }
  return null;
}


function hasRolesMiddleware(roles : string[], to : RouteLocationNormalized, from : RouteLocationNormalized, next: NavigationGuardNext) {
  checkAuthentication().then(authenticated => {
    if (!authenticated) {
      router.push("/login");
      return;
    }
    if (hasRoles(roles)){
      next();
      return;
    }
    router.push("/");
  })
}

function hasRoles(roles : string[]) : boolean {
  if (!localStorage.getItem("token")) {
    router.push("/login");
    return;
  }
  const token = localStorage.getItem("token");
  const decoded = jwtDecode(token);
  return roles.includes(decoded.role);
}

function getRole(){
  if (localStorage.getItem("token")) {
    const token = localStorage.getItem("token");
    const decoded = jwtDecode(token);
    return decoded.role;
  }
  return null;
}


function isLogged(){
  return !!localStorage.getItem("token");
}

// function hasRole(){
//
// }

export default {
  login,
  getAll,
  register,
  logout,
  isLogged,
  checkAuthenticationMiddleware,
  getCurrentUserId,
  hasRoles,
  hasRolesMiddleware,
  getRole,
  deleteUser
} as const;
