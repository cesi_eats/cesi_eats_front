import axios from "axios";
import {
  DeliverOrderResponse, Order, OrderResponse
} from "@/services/types/DataTypes";
import userService from "@/services/user-service";


async function getAvailableOrders() : Promise<Order[]> {
  const {data} = await axios.get<OrderResponse>("http://localhost:3000/api/order/available",
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return [];
}

async function getOrdersByUserId(userId : number) : Promise<Order[]> {
  const {data} = await axios.get<OrderResponse>("http://localhost:3000/api/order/user/" + userId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return [];
}

async function getOrdersByDelivererId(delivererId : number) : Promise<Order[]> {
  const {data} = await axios.get<OrderResponse>("http://localhost:3000/api/order/deliverer/" + delivererId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return [];
}

async function acceptOrder(orderId : string) : Promise<boolean> {
  const {data} = await axios.post<DeliverOrderResponse>("http://localhost:3000/api/order/accept/",
    {
      "orderId": orderId,
      "delivererId": userService.getCurrentUserId()
    },
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!data.success;

}

async function deliverOrder(orderId : string) : Promise<Order|null> {
  const {data} = await axios.get<DeliverOrderResponse>("http://localhost:3000/api/order/deliver/" + orderId,
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  if (data.success && data.data) {
    return data.data;
  }
  return null;
}

async function createOrder(articlesInCart : Map<string,number>, restaurantId : string, address : string) : Promise<boolean> {
  const {data} = await axios.post<OrderResponse>("http://localhost:3000/api/order/",
    {
      "articles": articlesInCart,
      "userId": userService.getCurrentUserId(),
      "restaurantId": restaurantId,
      "address": address
    },
    {
      validateStatus: function (status) {
        return status >= 200 && status < 500
      }
    });
  return !!data.success;

}

export default {
  getAvailableOrders,
  getOrdersByUserId,
  getOrdersByDelivererId,
  deliverOrder,
  acceptOrder,
  createOrder
} as const;
