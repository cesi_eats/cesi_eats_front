import {createStore, Store, useStore} from "vuex";
import { InjectionKey } from 'vue'

// declare your own store states
export interface State {
  currentRestaurantId: string;
  isLoggedIn: boolean;
  role: string;
  articlesInCart: Map<string, number>;
}


// define injection key
export const key: InjectionKey<Store<State>> = Symbol()


export const store = createStore<State>({
  state: {

    currentRestaurantId: "",
    isLoggedIn: false,
    role: "",
    articlesInCart: new Map<string, number>(),
  },
  mutations: {
    setIsLoggedIn(state, isLoggedIn) {
      state.isLoggedIn = isLoggedIn;
    },
    setRole(state, role) {
      state.role = role;
    },

  },
  actions: {
    setIsLoggedIn({commit}, isLoggedIn) {
      commit('setIsLoggedIn', isLoggedIn);
    },
    setRole({commit}, role) {
      commit('setRole', role);
    }
  },
  getters: {},
});

export function getStore(){
  return useStore(key);
}

// export let restaurantID = null;
