import { RouteRecordRaw, createRouter, createWebHashHistory } from 'vue-router'
import userService from "@/services/user-service";

const routes: Array<RouteRecordRaw> = [

  // {
  //   path: '/',
  //   name: 'Landing',
  //   component: () => import(/* webpackChunkName: "privacy" */ '../views/landing/index.vue'),
  // },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/about/index.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/login/index.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import(/* webpackChunkName: "login" */ '../views/register/index.vue'),
  },
  {
    path: '/privacy',
    name: 'Privacy',
    component: () => import(/* webpackChunkName: "privacy" */ '../views/privacy/index.vue'),
  },
  {
    path: '/register-restaurant',
    name: 'RegisterRestaurant',
    beforeEnter: (to, from, next) => {
      userService.hasRolesMiddleware(["owner","admin"],to, from, next);
    },
    component: () => import(/* webpackChunkName: "privacy" */ '../views/register-restaurant/index.vue'),
  },
  {
    path: '/menu',
    name: 'Menu',
    beforeEnter: (to, from, next) => {
      userService.checkAuthenticationMiddleware(to, from, next);
    },
    component: () => import(/* webpackChunkName: "privacy" */ '../views/menu/index.vue'),
  },
  {
    path: '/restaurants',
    name: 'Restaurants',
    beforeEnter: (to, from, next) => {
      userService.checkAuthenticationMiddleware(to, from, next);
    },
    component: () => import(/* webpackChunkName: "privacy" */ '../views/restaurants/index.vue'),
  },
  {
    path: '/orders',
    name: 'OrdersList',
    component: () => import(/* webpackChunkName: "privacy" */ '../views/orders-list/index.vue'),
  },
  {
    path: '/orders-deliver',
    name: 'OrdersDeliver',
    beforeEnter: (to, from, next) => {
      userService.hasRolesMiddleware(["deliverer","admin"],to, from, next);
    },
    component: () => import(/* webpackChunkName: "privacy" */ '../views/orders-deliver/index.vue'),
  },
  {
    path: '/users',
    name: 'UsersList',
    beforeEnter: (to, from, next) => {
      userService.hasRolesMiddleware(["admin"],to, from, next);
    },
    component: () => import(/* webpackChunkName: "privacy" */ '../views/users-list/index.vue'),
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
