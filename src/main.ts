import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
// import Buefy from 'buefy';
import "buefy/dist/buefy.css";
import axios from 'axios';
import {store, key} from "@/stores/main";
import vmfPlugin from "vue-final-modal";

axios.interceptors.request.use(
  (request: never) => {
    if (localStorage.getItem('token')) {
      request.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
    }
    return request;
  },
  (error: never) => {
    return Promise.reject(error);
  });

createApp(App).use(router).use(createPinia()).use(store,key).use(vmfPlugin).mount('#app')
